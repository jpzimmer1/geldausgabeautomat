/*
 * Fiducia IT AG, All rights reserved. Use is subject to license terms.
 */

package f10_stueckelung.version1;

import static java.lang.Integer.MAX_VALUE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;

public class StueckelungTest {

	private static final int[] ALLE_MOEGLICHEN_SCHEINE = new int[] { 200, 100, 50, 20, 10, 5 };
	private final Geldautomat sut = new Geldautomat();

	@Test
	public void beiFuenfEuroAuszahlungsWunsch_ErhaltenWirEinenFuenfEuroSchein() {
		sut.befuelleMax(ALLE_MOEGLICHEN_SCHEINE);
		assertThat(sut.berechneStueckelung(5), is(new int[] { 5 }));
	}

	@Test
	public void beiFuenfzehnEuroAuszahlungsWunsch_ErhaltenWirEinen10UndEinenFuenfEuroSchein() {
		sut.befuelleMax(ALLE_MOEGLICHEN_SCHEINE);
		assertThat(sut.berechneStueckelung(15), is(new int[] { 10, 5 }));
	}

	@Test
	public void beiFuenfundvierzigEuroAuszahlungsWunsch_ErhaltenWirZwei20UndEinenFuenfEuroSchein() {
		sut.befuelleMax(ALLE_MOEGLICHEN_SCHEINE);
		assertThat(sut.berechneStueckelung(45), is(new int[] { 20, 20, 5 }));
	}

	@Test
	public void beiNuerFuenfEuroScheinenWerdenBei10EuroZweiFuenfEuroScheineAusgezahlt() {
		Geldautomat sut = new Geldautomat();
		sut.befuelleMax(200, 100, 50, 20, 5);
		assertThat(sut.berechneStueckelung(10), is(new int[] { 5, 5 }));
	}

	@Test
	@Ignore
	public void wenn20igerWaehrendAuszahlungLeerWird_WirdMitNaechstMoeglichemScheinAusbezahlt() {
		Geldautomat sut = new Geldautomat();
		// hier haben wir ausversehen 1,20 geschrieben, da nicht typisiert. TODO
		// Typisieren, z.B. Schein.ZWANZIG, 1 sein
		sut.befuelle(20, 1);
		sut.befuelle(5, MAX_VALUE);
		assertThat(sut.berechneStueckelung(40), is(new int[] { 20, 5, 5, 5, 5 }));
	}

}
