/*
 * Fiducia IT AG, All rights reserved. Use is subject to license terms.
 */

package f10_stueckelung.version1;

import static java.lang.Integer.MAX_VALUE;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Geldautomat {

	private final Map<Integer, Integer> inhalt = new TreeMap<Integer, Integer>(
			Comparator.<Integer> naturalOrder().reversed());

	public void befuelleMax(int... values) {
		for (int schein : values) {
			befuelle(schein, MAX_VALUE);
		}
	}

	public void befuelle(int schein, int anzahl) {
		inhalt.put(schein, anzahl);
	}

	int[] berechneStueckelung(int auszahlungswunsch) {
		int restbetrag = auszahlungswunsch;
		List<Integer> ergebnis = new ArrayList<Integer>();
		for (Entry<Integer, Integer> entry : inhalt.entrySet()) {
			int groesstMoeglichVerfuegbareSchein = entry.getKey();
			while (restbetrag >= groesstMoeglichVerfuegbareSchein) {
				ergebnis.add(groesstMoeglichVerfuegbareSchein);
				restbetrag -= groesstMoeglichVerfuegbareSchein;
			}
		}
		return ergebnis.stream().mapToInt(i -> i).toArray();
	}

}
