/*
 * Fiducia IT AG, All rights reserved. Use is subject to license terms.
 */

package f10_stueckelung.version2;

import static java.lang.Integer.MAX_VALUE;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Geldautomat {

	public enum Schein {
		ZWEIHUNDERT(200), HUNDERT(100), FUENFZIG(50), ZWANZIG(20), ZEHN(10), FUENF(5);

		private int wert;

		private Schein(int wert) {
			this.wert = wert;
		}

		public int getWert() {
			return wert;
		}

	}

	private final Map<Schein, Integer> inhalt = new TreeMap<Schein, Integer>(
			Comparator.<Schein> naturalOrder().reversed());

	public void befuelleMax(Schein... values) {
		for (Schein schein : values) {
			befuelle(schein, MAX_VALUE);
		}
	}

	public void befuelle(Schein schein, int anzahl) {
		inhalt.put(schein, anzahl);
	}

	int[] berechneStueckelung(int auszahlungswunsch) {
		int restbetrag = auszahlungswunsch;
		List<Integer> ergebnis = new ArrayList<Integer>();
		for (Entry<Schein, Integer> entry : inhalt.entrySet()) {
			int groesstMoeglichVerfuegbareSchein = entry.getKey().getWert();
			while (restbetrag >= groesstMoeglichVerfuegbareSchein) {
				ergebnis.add(groesstMoeglichVerfuegbareSchein);
				restbetrag -= groesstMoeglichVerfuegbareSchein;
			}
		}
		return ergebnis.stream().mapToInt(i -> i).toArray();
	}

}
