/*
 * Fiducia IT AG, All rights reserved. Use is subject to license terms.
 */

package f10_stueckelung.version2;

import static java.lang.Integer.MAX_VALUE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;

import f10_stueckelung.version2.Geldautomat.Schein;

public class StueckelungTest {

	private static final Schein[] ALLE_MOEGLICHEN_SCHEINE = Schein.values();
	private final Geldautomat sut = new Geldautomat();

	@Test
	public void beiFuenfEuroAuszahlungsWunsch_ErhaltenWirEinenFuenfEuroSchein() {
		sut.befuelleMax(ALLE_MOEGLICHEN_SCHEINE);
		assertThat(sut.berechneStueckelung(5), is(new int[] { 5 }));
	}

	@Test
	public void beiFuenfzehnEuroAuszahlungsWunsch_ErhaltenWirEinen10UndEinenFuenfEuroSchein() {
		sut.befuelleMax(ALLE_MOEGLICHEN_SCHEINE);
		assertThat(sut.berechneStueckelung(15), is(new int[] { 10, 5 }));
	}

	@Test
	public void beiFuenfundvierzigEuroAuszahlungsWunsch_ErhaltenWirZwei20UndEinenFuenfEuroSchein() {
		sut.befuelleMax(ALLE_MOEGLICHEN_SCHEINE);
		assertThat(sut.berechneStueckelung(45), is(new int[] { 20, 20, 5 }));
	}

	@Test
	public void beiNuerFuenfEuroScheinenWerdenBei10EuroZweiFuenfEuroScheineAusgezahlt() {
		Geldautomat sut = new Geldautomat();
		sut.befuelleMax(Schein.ZWEIHUNDERT, Schein.HUNDERT, Schein.FUENFZIG, Schein.ZWANZIG, Schein.FUENF);
		assertThat(sut.berechneStueckelung(10), is(new int[] { 5, 5 }));
	}

	@Test
	@Ignore
	public void wenn20igerWaehrendAuszahlungLeerWird_WirdMitNaechstMoeglichemScheinAusbezahlt() {
		Geldautomat sut = new Geldautomat();
		// hier haben wir ausversehen 1,20 geschrieben, da nicht typisiert. TODO
		// Typisieren, z.B. Schein.ZWANZIG, 1 sein
		sut.befuelle(Schein.ZWANZIG, 1);
		sut.befuelle(Schein.FUENF, MAX_VALUE);
		assertThat(sut.berechneStueckelung(40), is(new int[] { 20, 5, 5, 5, 5 }));
	}

}
